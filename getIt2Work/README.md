Right now there are a lot of pieces that all need to fit together
which is still better than having no pieces at all

The best thing to do now is to go through the gazebo plugin tutorial again,
maybe create the velodyne robot, and move it with Gazebo, then connect it to ROS
with the Gazebo Connect to ROS tutorial.

After that, create the a ROS package from stratch, create the velodyne plugin 
in that package, and use that package to simulate the world

ROS ConstructSIM #19 on Creating Gazebo Plugins will be a good reference as well
as the inverted_pendulum_plugin created from ConstructSIM's Q&A on Creating plugins