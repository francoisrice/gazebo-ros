#include <gazebo/gazebo_config.h>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>

// Make sure to handle API differences between gazebo's major versions
#if GAZEBO_MAJOR_VERSION < 6
#include <gazebo/gazebo.hh>
#else
#include <gazebo/gazebo_client.hh>
#endif

/////////
int main(int _argc, char **_argv){
    // Load gazebo
#if GAZEBO_MAJOR_VERSION < 6
    gazebo::setupClient(_argc, _argv);
#else
    gazebo::client::setup(_argc, _argv);
#endif

    //Create a node for communication
    gazebo::transport::NodePtr node(new gazebo::transport::Node());
    node->Init();

    // Pub to the plugin topic
    gazebo::transport::PublisherPtr pub = 
       node->Advertise<gazebo::msgs::Vector3d>("~/my_robot/vel_cmd");

    // Wait for a Sub to connect to the Pub
    pub->WaitForConnection();

    // Create a vector3 message
    gazebo::msgs::Vector3d msg;

    // Set the velocity in the x-component
    #if GAZEBO_MAJOR_VERSION < 6
        gazebo::msgs::Set(&msg, gazebo::math::Vector3(std::atof(_argv[1]), 0, 0));
        // gazebo::msgs::Set(&msg, gazebo::math::Vector3(std::atof(_argv[1]), velRight?, 0));
    #else
        gazebo::msgs::Set(&msg, ignition::math::Vector3d(std::atof(_argv[1]), 0, 0));
    #endif

    // Send the message
    pub->Publish(msg);

    // Clean up when everything's done
    #if GAZEBO_MAJOR_VERSION < 6
        gazebo::shutdown();
    #else
        gazebo::client::shutdown();
    #endif
}