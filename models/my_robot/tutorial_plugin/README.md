This directory contains code adapted from Gazebo's [Control Plugin](https://gazebosim.org/tutorials?cat=guided_i&tut=guided_i5) tutorial. IN Gazebo's tutorial, the plugin was designed to control the rotation of the Velodyne camera model. This tutorial adaptation is meant to control and manipulate the two-wheeled robot from the [other Gazebo tutorials.](https://gazebosim.org/tutorials) 
Things to note: 
---
1. If you are using ROS with gazebo to control robots, you should have ROS installed already before creating the plugin. This is because different versions of ROS only work with specific versions of gazebo. 
    - To "install" ROS you need only add its keyserver to your terminal, by following the first 4 steps [here.](http://wiki.ros.org/kinetic/Installation/Ubuntu) After entering the `sudo apt-key adv` command and running `sudo apt update`, you are ready to use ROS packages. You do not need to install `ros-kinetic-desktop` or any of the other package options, unless you do not know which packages will be necessary. These instructions are for ROS Kinetic on Ubuntu, and should be adapted if needed for another version of ROS.
2. In the `.world` file, one needs to define the robot that is being simulated in the gazebo world, and define the name of the `.so` file to be created during the `cmake` process.

---

To summarize the key points needed to create a plugin:
1. Create the folder for the plugin -> `mkdir plugin`
2. Create the plugin source file -> `code plugin.cc` and tailor the code to work for your application
3. Create the CMake build script -> `CMakeLists.txt` to build the source files
4. Create the `plugin.world` file to create the gazebo environment with the robot and plugin inside 
5. Build the scripts:
```
mkdir build
cd build
cmake ..
make -j4
gazebo --verbose ../tutorial.world
```

If you created/loaded the robot, and coded the plugin successfully your terminal should print:
`The velodyne plugin is attached to model[my_velodyne]`

Whenever an edit is made to the `plugin.cc` file, run the `make` command from the `/build` directory.

Whenever an edit is made to the `plugin.world` file, from the `/build` directory, run:

```
cmake ..
make -j4
gazebo --verbose ../plugin.world
```

Important Note: For models to be used/loaded into a world, the model must be in `~/.gazebo/models/`
---

Creating a Gazebo Plugin with ROS
0. Make sure ROS is initilized in the terminal by running
    `source /opt/ros/kinetic/setup.bash`
1. Create a catkin workspace
 - `mkdir project_ws/src`
 - `cd project_ws/src`
 - `catkin_init_workspace`
 This creates the catkin workspace for your project, and generates a 
 CMakeLists.txt file in the `src` folder.
 - `cd ..`
 - `catkin_make`
 This will create a `build` and `devel` folder for executables to run in your 
 project. 
 To initialize this project workspace, and run the functions/package within, run
 `source devel/setup.bash`
2. Create the ROS package
  - `cd project_ws/src`
  - `catkin_create_pkg plugin_name roscpp gazebo_ros`
 A new plugin file named `plugin_name` will be created within the `src` folder,
 with it's own `CMakeLists.txt`, `package.xml`, `include/plugin_name`, and `src/`
3. Modify the created CMakeLists.txt
  - add `find_package(gazebo REQUIRED)` after the `find_package(catkin ...)`
  - add `link_directories(${GAZEBO_LIBRARY_DIRS})` at the top of the build section
  - add `${Boost_INCLUDE_DIR} ${GAZEBO_INCLUDE_DIRS}` to the `include_directories($...)`
  - add `${GAZEBO_LIBRARIES}` to the `target_link_libraries()`
  - uncomment `CATKIN_DEPENDS` (and maybe change to DEPENDS, unconfirmed)
4. Have the package xml file look for the created plugin
  - At the end of `package.xml` add 
  ```
  <export>
    <gazebo_ros plugin_path="${prefix}/lib" gazebo_media_path="${prefix}" />
  </export> 
  ```
Once the changes are made, go to the project workspace folder and run
 - `catkin_make` 
After compiling completes without error, the plugin can be added to a `.world`
file as `<plugin name="project_name" filename="plugin_name.so">` and run in gazebo

To run the world with ROS, create a `.launch` file:
```
<?xml version="1.0" encoding="UTF-8"?>
<launch>
    <include file="$(find gazebo_ros)/launch/empty_world.launch">
        <arg name="world_name" value="$(find project_name)/world_name.world" />
    </include>
</launch>
```