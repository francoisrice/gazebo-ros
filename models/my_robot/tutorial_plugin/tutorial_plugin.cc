#ifndef _TUTORIAL_PLUGIN_HH_
#define _TUTORIAL_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>

/* Note: A plugin is the processing software behind any 
    component. Therefore, plugins are needed for sensors, 
    and actuators.
*/

namespace gazebo {
    // Plugin to control the robot, my_robot. This plugin works with a velocity 
    // set in the tutorial.world <plugin> tag or without a velocity set
    class TutorialPlugin : public ModelPlugin {
        public: TutorialPlugin(){}; // plugin constructor

        public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf){
                if (_model->GetJointCount() == 0){
                    std::cerr << "Joint Count is 0; Tutorial Plugin not loaded\n";
                    return;
                }

                // Import the model, as this objects
                this->model = _model; // Couldn't we just use _model though?

                // Get the first joint. It is assumed that is the left wheel of the
                //   model as it is declared first in the model.sdf, so
                //   this needs to be duplicated for the right wheel, and renamed
                //   to distinguish between the two models.
                this->joint = _model->GetJoints()[0];

                this->jointRight = _model->GetJoints()[1]

                // Set the PID controller with only P-gain
                this->pid = common::PID(0.1, 0, 0);

                // Apply the PID controller to the model's first joint 
                this->model->GetJointController()->SetVelocityPID(
                    this->joint->GetScopedName(), this->pid);
                
                this->model->GetJointController()->SetVelocityPID(
                    this->jointRight->GetScopedName(), this->pid);

                // Declare 0 initial velocity;
                double velocity = 0; // double is used from tutorial
                                    // A float should work just fine(?)

                // This block retries the velocity of the model from the world file
                if (_sdf->HasElement("velocity")){
                    velocity = _sdf->Get<double>("velocity"); 
                } else {
                    velocity = 0.1;
                }

                this->SetVelocity(velocity);
                
                //this->SetVelocity(velocity,velocity); // Rewrite setVelocity to 
                                                        // set the velocity of both
                                                        // wheels

                // Create the Node for the Pub/Sub pair API
                this->node = transport::NodePtr(new transport::Node());
                #if GAZEBO_MAJOR_VERSION < 8
                this->node->Init(this->model->GetWorld()->GetName());
                #else
                this->node->Init(this->model->GetWorld()->Name());
                #endif

                // Create a topic name
                std::string topicName = "~/" + this->model->GetName() + "/vel_cmd";
                
                // Try this 
                // std::string topicName = "~/my_robot/vel_cmd";

                // Subscribe to the topic, and register a callback
                this->sub = this->node->Subscribe(topicName, &TutorialPlugin::OnMsg, this);

                // Set the velocity for this joint -> This is made obsolete by the SetVelocity function
                // this->model->GetJointController()->SetVelocityTarget(
                //     this->joint->GetScopedName(), velocity);
            };

        public:
          void SetVelocity(const double &_vel)
          //   void SetVelocity(const double &_vel, const double &_velRight)
          {
              // This is the implementation of the functional API

              // Set the joint's target velocity.
              this->model->GetJointController()->SetVelocityTarget(
                  this->joint->GetScopedName(), _vel);

            //   this->model->GetJointController()->SetVelocityTarget(
            //       this->jointRight->GetScopedName(), _velRight);
        };

        /// \brief Callback function to handle incoming messages
        /// \param[in] _msg Repurpose a vector3 message, and
        /// only use the x component of the function.
        private: void OnMsg(ConstVector3dPtr &_msg){
            this->SetVelocity(_msg->x());
            // this->SetVelocity(_msg->x(),_msg->y());
        }

        /// \brief A node used for transport
        private: transport::NodePtr node;

        /// \brief A subscriber to a named topic
        private: transport::SubscriberPtr sub;

        private: physics::ModelPtr model;

        private: physics::JointPtr joint;

        private: common::PID pid;
    };




        /* The load function is called by Gazebo when the plugin is inserted into
        the simulation.
        \param[in] _model: A pointer to the model that this plugin is attached
        to.
        \param[in] _sdf: A pointer to the plugin's SDF element.
        */

        /// \brief Set the velocity of the (not) Velodyne
        /// \param[in] _vel New target velocity


    // This line let's Gazebo know to use this plugin and call the Load function
    GZ_REGISTER_MODEL_PLUGIN(TutorialPlugin)
}
#endif
