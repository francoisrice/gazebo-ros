#ifndef _TUTORIAL_PLUGIN_HH_
#define _TUTORIAL_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>

namespace gazebo
{
/// \brief Basic plugin to spin a single joint machine to a set velocity.
/// This version of the plugin uses the functional API implementation to set
/// the joint's velocity to what's passed into the function.
/// The functional API is good when interfacing Gazebo with ROS, but as is
/// this plugin is unable to control a robot. Another plugin would need to 
/// inherit this plugin and use SetVelocity() to set the joint's velocity.
class TutorialPlugin : public ModelPlugin
{
    /// \brief Constructor
  public:
    TutorialPlugin() {}

    /// \brief The load function is called by Gazebo when the plugin is
    /// inserted into simulation
    /// \param[in] _model A pointer to the model that this plugin is
    /// attached to.
    /// \param[in] _sdf A pointer to the plugin's SDF element.
  public:
    virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
    {
        // Safety check
        if (_model->GetJointCount() == 0)
        {
            std::cerr << "Invalid joint count, Velodyne plugin not loaded\n";
            return;
        }

        // Store the model pointer for convenience.
        this->model = _model;

        // Get the first joint. We are making an assumption about the model
        // having one joint that is the rotational joint.
        this->joint = _model->GetJoints()[0];

        // Setup a P-controller, with a gain of 0.1.
        this->pid = common::PID(0.1, 0, 0);

        // Apply the P-controller to the joint.
        this->model->GetJointController()->SetVelocityPID(
            this->joint->GetScopedName(), this->pid);

        // Declare & Set the joint's initial velocity to 0
        double velocity = 0;

        this->SetVelocity(velocity);
    }

    /// \brief Set the velocity of the Velodyne
    /// \param[in] _vel New target velocity
  public:
    void SetVelocity(const double &_vel)
    {
      // Set the joint's target velocity.
      this->model->GetJointController()->SetVelocityTarget(
          this->joint->GetScopedName(), _vel);
    }

    /// \brief Pointer to the model.
  private:
    physics::ModelPtr model;

    /// \brief Pointer to the joint.
  private:
    physics::JointPtr joint;

    /// \brief A PID controller for the joint.
  private:
    common::PID pid;
};

// Tell Gazebo about this plugin, so that Gazebo can call Load on this plugin.
GZ_REGISTER_MODEL_PLUGIN(TutorialPlugin)
} // namespace gazebo
#endif