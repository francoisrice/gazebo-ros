# CMake generated Testfile for 
# Source directory: /home/francois/code/Cpp/ROS/gazebo-ros/models/my_robot/inverted_pendulum_ws/src
# Build directory: /home/francois/code/Cpp/ROS/gazebo-ros/models/my_robot/inverted_pendulum_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(inverted_pendulum_plugin)
