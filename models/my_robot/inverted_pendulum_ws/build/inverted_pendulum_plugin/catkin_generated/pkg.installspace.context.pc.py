# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "".split(';') if "" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;gazebo_ros".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "inverted_pendulum_plugin"
PROJECT_SPACE_DIR = "/home/francois/code/Cpp/ROS/gazebo-ros/models/my_robot/inverted_pendulum_ws/install"
PROJECT_VERSION = "0.0.0"
