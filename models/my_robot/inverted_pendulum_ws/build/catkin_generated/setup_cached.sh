#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export PKG_CONFIG_PATH="/home/francois/code/Cpp/ROS/gazebo-ros/models/my_robot/inverted_pendulum_ws/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/francois/code/Cpp/ROS/gazebo-ros/models/my_robot/inverted_pendulum_ws/build"