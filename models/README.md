The my_robot folder contains all the elements needed for a gazebo robot project, taken from the ["Gazebo: Make a Mobile Robot" tutorial](https://gazebosim.org/tutorials?tut=build_robot&cat=build_robot). 

[Gazebo: Control Plugin](http://gazebosim.org/tutorials?cat=guided_i&tut=guided_i5)
[Gazebo: Connect to ROS](http://gazebosim.org/tutorials?cat=guided_i&tut=guided_i6)